package usaTelegrama;
import java.io.IOException;
import muyutil.Miscelanea;

public class Telegrama {
private String texto;//el texto del telegrama
private String tipo;//tipo de telegram ordinario o urgente
public void setTipo(String tipo) {
this.tipo = tipo;
}
public void setTexto(String texto) {
this.texto = texto;
}

public String getCoste() {
int numPalabras = texto.split(" ").length;
double coste = 0;
double tarifa1;
double tarifa2;
if (tipo.equalsIgnoreCase("ordinario")) {
tarifa1 = 0.25;
tarifa2 = 0.05;
} else {
tarifa1 = 0.4;
tarifa2 = 0.075;
}
if (numPalabras <= 10) {
coste = tarifa1 * numPalabras;
} else {
coste = tarifa1 * 10 + tarifa2 * (numPalabras - 10);
}
return Miscelanea.euro(coste);
}
public String toString() {
int numPalabras = texto.split(" ").length;
return texto + "\n(" + tipo + " / " + numPalabras + " / " + getCoste();
}
public static void main(String[] args) throws IOException {
Telegrama telegrama = new Telegrama();
telegrama.setTexto(Miscelanea.leeCadena("Entra el texto:"));
telegrama.setTipo(Miscelanea.leeCadena("Entra el tipo Ordinario / Urgente:"));
System.out.println("el telegrama vale " + telegrama.getCoste());
System.out.println("El telegrama es\n" + telegrama);
}
}